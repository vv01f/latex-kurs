# Einführung in LaTeX

Dies sind die LaTeX Quelldateien für den Kurs *Einführung in LaTeX*, der
regelmäßig an der [HTW Dresden](htw-dresden.de) gehalten wird.  Alle Inhalte
sind, sofern nicht anders angegeben, unter
einer [Creative Commons Attribution-ShareAlike 3.0 Unported License](CC-BY-SA)
lizensiert.

[CC-BY-SA]: http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
